This is a composer based installer for developers working on the [Drutopia distribution](http://www.drupal.org/project/drutopia).

## Prerequisites

The [development environment of your choice](http://docs.drutopia.org/en/latest/quickstart.html) or simply PHP (7.2+ preferred) and [Composer](https://getcomposer.org/download/) (preferably [installed globally](https://getcomposer.org/doc/00-intro.md#globally)).


## Installation of Drutopia for development

No need to download this project; Packagist will handle that for you.

```
composer create-project drutopia/drutopia_dev_template:dev-master --no-interaction --keep-vcs drutopia-dev-site
```

Composer will create a new directory called my-drutopia-dev-site (change to whatever name you would like). Inside you will find the web directory with the entire code base of [Drutopia distribution](http://www.drupal.org/project/drutopia). You should be able to install it like any other Drupal site.

To use PHP's built-in webserver and SQLite:

```
cd drutopia-dev-site/web
php -S localhost:8008
```

Then open [localhost:8008](http://localhost:8008) in your browser and follow Drupal's installation instructions, leaving database connection information at its default.

If at any point you want to delete your database so you can start development fresh with a new install you can `rm web/sites/default/files/.ht.sqlite`.

# Updating Drutopia for development

```
cd drutopia-dev-site
git pull
composer update
```

And likely deleting the database and starting fresh, or at least running `drush updb`.
